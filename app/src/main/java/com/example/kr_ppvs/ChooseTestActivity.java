package com.example.kr_ppvs;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.kr_ppvs.database.AppDatabase;

public class ChooseTestActivity extends AppCompatActivity {

    AppDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_test);

        db = new AppDatabase(this);
    }
}
