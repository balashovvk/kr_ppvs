package com.example.kr_ppvs;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.main_addTest)
    AppCompatButton addButton;
    @BindView(R.id.main_startTest)
    AppCompatButton startButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        // Лямбда выражение
        startButton.setOnClickListener(v ->
                startActivity(new Intent(MainActivity.this, ChooseTestActivity.class)));
        addButton.setOnClickListener(v ->
                startActivity(new Intent(MainActivity.this, TestCreationActivity.class)));
    }
}
