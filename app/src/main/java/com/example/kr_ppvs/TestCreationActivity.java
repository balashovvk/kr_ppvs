package com.example.kr_ppvs;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import com.example.kr_ppvs.database.dto.QuestionDTO;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TestCreationActivity extends AppCompatActivity {

    @BindView(R.id.testCreation_addQuestion)
    AppCompatButton addQuestion;
    @BindView(R.id.testCreation_list)
    ListView listView;
    private final int REQUEST_CREATE_QUESTION = 0;
    ListAdapter listAdapter = new ListAdapter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_creation);
        ButterKnife.bind(this);

        listView.setAdapter(listAdapter);
        addQuestion.setOnClickListener(v -> {
            startActivityForResult(new Intent(TestCreationActivity.this, CreateQuestionActivity.class), REQUEST_CREATE_QUESTION);
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CREATE_QUESTION) {
                listAdapter.addQuestion(
                        data.getStringExtra("Question"),
                        data.getStringExtra("Answer1"),
                        data.getStringExtra("Answer2"),
                        data.getStringExtra("Answer3"),
                        data.getStringExtra("Answer4")
                );
            }
        }
    }

    class ListAdapter extends BaseAdapter {

        ArrayList<QuestionDTO> questions = new ArrayList<>();
        ViewHolder holder;

        @Override
        public int getCount() {
            return questions.size();
        }

        @Override
        public QuestionDTO getItem(int position) {
            return questions.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            if (view != null) {
                holder = (ViewHolder) view.getTag();
            } else {
                view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.item_question, parent, false);
                holder = new ViewHolder(view);
                view.setTag(holder);
            }
            holder.question.setText(questions.get(position).getQuestion());
            holder.answer1.setText(questions.get(position).getAnswers().get(0));
            holder.answer2.setText(questions.get(position).getAnswers().get(1));
            holder.answer3.setText(questions.get(position).getAnswers().get(2));
            holder.answer4.setText(questions.get(position).getAnswers().get(3));
            return view;
        }

        void addQuestion(String question, String answer1, String answer2, String answer3, String answer4) {
            questions.add(new QuestionDTO(question, answer1, answer2, answer3, answer4));
            notifyDataSetChanged();
        }

        class ViewHolder {
            @BindView(R.id.question)
            TextView question;
            @BindView(R.id.answer1)
            TextView answer1;
            @BindView(R.id.answer2)
            TextView answer2;
            @BindView(R.id.answer3)
            TextView answer3;
            @BindView(R.id.answer4)
            TextView answer4;

            ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }
    }
}
