package com.example.kr_ppvs.database.model;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity(foreignKeys = {
        @ForeignKey(entity = User.class, parentColumns = "id", childColumns = "userId"),
        @ForeignKey(entity = Result.class, parentColumns = "id", childColumns = "resultId")
})
public class UserResult {
    @PrimaryKey(autoGenerate = true)
    public Long id;
    public Long userId;
    public Long resultId;
}
