package com.example.kr_ppvs.database.model;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity
@ForeignKey(entity = Test.class, parentColumns = "id", childColumns = "testId")
public class Question {
    @PrimaryKey(autoGenerate = true)
    public Long id;
    public Long testId;
    public String questionText;
    public String answer1;
    public String answer2;
    public String answer3;
    public String answer4;
}
