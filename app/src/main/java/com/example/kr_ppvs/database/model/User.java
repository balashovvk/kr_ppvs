package com.example.kr_ppvs.database.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class User {
    @PrimaryKey(autoGenerate = true)
    public Long id;
    public String userName;
    public String password;
    public String email;
}
