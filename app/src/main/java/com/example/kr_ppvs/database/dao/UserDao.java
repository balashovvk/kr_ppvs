package com.example.kr_ppvs.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.kr_ppvs.database.model.User;

import java.util.List;

@Dao
public interface UserDao {

    @Insert
    void addUser(User user);

    @Delete
    void delete(User user);

    @Query("Select * from User")
    List<User> getAll();

}
