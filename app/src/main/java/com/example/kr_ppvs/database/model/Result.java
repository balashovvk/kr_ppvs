package com.example.kr_ppvs.database.model;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity
@ForeignKey(entity = Test.class, parentColumns = "id", childColumns = "testId")
public class Result {
    @PrimaryKey(autoGenerate = true)
    public Long id;
    public Long testId;
    public String resultText;
}
