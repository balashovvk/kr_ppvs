package com.example.kr_ppvs.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.kr_ppvs.database.model.Test;

import java.util.List;

@Dao
public interface TestDao {

    @Insert
    void addTest(Test test);

    @Delete
    void delete(Test test);

    @Query("Select * from Test")
    List<Test> getAll();

}
