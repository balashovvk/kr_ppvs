package com.example.kr_ppvs.database;

import androidx.room.RoomDatabase;

import com.example.kr_ppvs.database.dao.TestDao;
import com.example.kr_ppvs.database.dao.UserDao;
import com.example.kr_ppvs.database.model.Question;
import com.example.kr_ppvs.database.model.Result;
import com.example.kr_ppvs.database.model.Test;
import com.example.kr_ppvs.database.model.User;
import com.example.kr_ppvs.database.model.UserResult;

@androidx.room.Database(
        entities = {User.class, Test.class, Question.class, Result.class, UserResult.class},
        version = 1,
        exportSchema = false
)
public abstract class Database extends RoomDatabase {
    public abstract UserDao userDao();
    public abstract TestDao testDao();
}
