package com.example.kr_ppvs.database;

import android.content.Context;

import androidx.room.Room;

import com.example.kr_ppvs.R;

public class AppDatabase {

    private Database database;

    public AppDatabase(Context context) {
        database = Room.databaseBuilder(context,
                Database.class, context.getString(R.string.database_name))
                .allowMainThreadQueries()
                .build();
    }
}
