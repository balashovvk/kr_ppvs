package com.example.kr_ppvs.database.model;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity
@ForeignKey(entity = User.class, parentColumns = "id", childColumns = "creatorId")
public class Test {
    @PrimaryKey(autoGenerate = true)
    public Long id;
    public Long creatorId;
    public String testName;
    public String description;
}
