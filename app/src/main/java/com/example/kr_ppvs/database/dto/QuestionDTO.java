package com.example.kr_ppvs.database.dto;

import java.util.ArrayList;
import java.util.Arrays;

public class QuestionDTO {

    private String question;

    private ArrayList<String> answers;

    public QuestionDTO(String question, String answer1, String answer2, String answer3, String answer4) {
        this.question = question;
        this.answers = new ArrayList<>(Arrays.asList(answer1, answer2, answer3, answer4));
    }

    public String getQuestion() {
        return question;
    }

    public ArrayList<String> getAnswers() {
        return answers;
    }
}
