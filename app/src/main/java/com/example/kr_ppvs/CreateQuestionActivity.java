package com.example.kr_ppvs;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CreateQuestionActivity extends AppCompatActivity {

    @BindView(R.id.create)
    AppCompatButton create;
    @BindView(R.id.question)
    TextView question;
    @BindView(R.id.answer1)
    TextView answer1;
    @BindView(R.id.answer2)
    TextView answer2;
    @BindView(R.id.answer3)
    TextView answer3;
    @BindView(R.id.answer4)
    TextView answer4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_question);
        ButterKnife.bind(this);
        create.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.putExtra("Question", question.getText().toString());
            intent.putExtra("Answer1", answer1.getText().toString());
            intent.putExtra("Answer2", answer2.getText().toString());
            intent.putExtra("Answer3", answer3.getText().toString());
            intent.putExtra("Answer4", answer4.getText().toString());
            setResult(RESULT_OK, intent);
            finish();
        });
    }
}
